import numpy as np
import tensorflow as tf

mnist = tf.contrib.keras.datasets.mnist
Sequential = tf.contrib.keras.models.Sequential 
Dense = tf.contrib.keras.layers.Dense
Dropout = tf.contrib.keras.layers.Dropout
Activation = tf.contrib.keras.layers.Activation
Adam = tf.contrib.keras.optimizers.Adam
from tensorflow.contrib.keras.utils import np_utils
from tf.contrib.keras import backend as K

def load_data(nb_classes=10):
    # the data, shuffled and split between tran and test sets
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    X_train = X_train.reshape(60000, 784)
    X_test = X_test.reshape(10000, 784)
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    X_train /= 255
    X_test /= 255
    print(X_train.shape[0], 'train samples')
    print(X_test.shape[0], 'test samples')

    # convert class vectors to binary class matrices
    y_train = np_utils.to_categorical(y_train, nb_classes)
    y_test = np_utils.to_categorical(y_test, nb_classes)

    return X_train, y_train, X_test, y_test

def mk_model():
    model = Sequential()
    model.add(Dense(512, input_dim=784))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(36))
    model.add(Activation('softmax'))

    return model


if __name__ == '__main__':
    np.random.seed(1337)        # for reproducibility
    batch_size = 128
    nb_epoch = 20

    X_train, y_train, X_test, y_test = load_data()
    model = mk_model()
    model.summary()             # check model configuration

    model.compile(loss='categorical_crossentropy',
                optimizer=Adam(),
                metrics=['accuracy'])

    model.fit(X_train, y_train,
          batch_size=batch_size, epochs=nb_epoch,
          verbose=1, 
          validation_data=(X_test, y_test))

    score = model.evaluate(X_test, y_test, verbose=0)
    print('\nTest score   : {:>.4f}'.format(score[0]))
    print('Test accuracy: {:>.4f}'.format(score[1]))

    K.clear_session()



